#!/usr/bin/env perl

use strict;
use warnings;

die "usage: $0 threshold file […]\n" unless $#ARGV > 0;

my $threshold = shift;

my $header = <>;
chomp $header;
my @headers = split /,/, $header;
while (<>) {
  chomp;
  my @parts = split /,/;
  for (my $i=1; $i <= $#headers; $i++) {
    if (defined $parts[$i]) {
      my $similarity = $parts[$i];
      print "$parts[0],$headers[$i]\n" if $similarity >= $threshold;
    }
  }
}
